module.exports = function(grunt) {

  // configure the tasks
  grunt.initConfig({

	cssmin: {
	  build: {
	    files: {
	      'danielecastaldo/css/application.css': [ 'danielecastaldo/css/*.css' ]
	    }
	  }
	}

  });

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // define the tasks
grunt.registerTask(
  'stylesheets', 
  'Compiles the stylesheets.', 
  [ 'cssmin' ]
);
};
